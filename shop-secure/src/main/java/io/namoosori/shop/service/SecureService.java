package io.namoosori.shop.service;

import java.util.Optional;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.namoosori.shop.entity.CustomUser;
import io.namoosori.shop.store.SecureStore;
import io.namoosori.shop.util.JwtUtil;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SecureService {
	private final SecureStore secureStore;
	private final PasswordEncoder passwordEncoder;
	private final JwtUtil jwtUtil;
	
	public String join(CustomUser customUser) {
		customUser.setPassword(passwordEncoder.encode(customUser.getPassword()));
		customUser.getRoles().add("USER");
		
		return secureStore.create(customUser);
	}
	
	public String login(CustomUser customUser) {
		CustomUser user = Optional.ofNullable(secureStore.retrieve(customUser.getEmail()))
				.orElseThrow(() -> new UsernameNotFoundException("No such memeber with" + customUser.getEmail()));
		if(!passwordEncoder.matches(customUser.getPassword(), user.getPassword())) {
			throw new IllegalArgumentException("Password not correct");
		}
		return jwtUtil.createAccessToken(user.getUsername(), user.getRoles());
				
	}
}
