package io.namoosori.shop.store;

import io.namoosori.shop.entity.CustomUser;

public interface SecureStore{
	String create(CustomUser customUser);
	CustomUser retrieve(String email);
}
