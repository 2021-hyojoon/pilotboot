package io.namoosori.shop.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.shop.entity.Member;
import io.namoosori.shop.service.MemberService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/member")
@RequiredArgsConstructor
public class MemberResource {
	private final MemberService memberService;
	
	
	@PostMapping("/register")
	public String register(@RequestBody Member member) {
		return memberService.register(member);
	}

	@GetMapping("/find")
	public Member find(@RequestParam String id) {
		return memberService.find(id);
	}
	
	@PutMapping("/modify")
	public void modify(@RequestBody Member member) {
		memberService.modify(member);
	}
	
	@DeleteMapping("/remove")
	public void remove(String id) {
		memberService.remove(id);
	}
}
