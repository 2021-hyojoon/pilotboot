package io.namoosori.shop.rest;


import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;
import io.namoosori.shop.service.ProductService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductResource {
	private final ProductService productService;
	
	@PostMapping("/register")
	public String register(@RequestBody Product product) {
		return productService.registerProduct(product);
	}

	@GetMapping("/find-name")
	public Product findByName(@RequestParam String productName) {
		return productService.findProductName(productName);
	}
	
	@GetMapping("/find")
	public Product find(@RequestParam String productId) {
		return productService.findProduct(productId);
	}
	
	@GetMapping("/find-by-category")
	public List<Product> findByCategory(@RequestParam Category category) {
		return productService.findByCategory(category);
	}
	
	@GetMapping("/find-all")
	public List<Product> findAllClub() {
		return productService.findAllClub();
	}
	
	@PutMapping("/modify")
	public void modify(@RequestBody Product product) {
		productService.modify(product);
	}
	
	@DeleteMapping("/remove")
	public void remove(String productId) {
		productService.remove(productId);
	}
}
