package io.namoosori.shop.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;
import io.namoosori.shop.service.OrderService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderResource {
	private final OrderService orderService;
	
	@PostMapping("/register")
	public String register(@RequestBody Order order) {
		return orderService.registerOrder(order);
	}

	@GetMapping("/find")
	public Order find(@RequestParam String orderId) {
		return orderService.findOrder(orderId);
	}
	
	@GetMapping("/find-all")
	public List<Order> findAll() {
		return orderService.findAll();
	}
	
	@PutMapping("/modify")
	public void modify(@RequestBody Order order) {
		orderService.modify(order);
	}
	
	@DeleteMapping("/remove")
	public void remove(String orderId) {
		orderService.remove(orderId);
	}
	
	@GetMapping("/find-by-order")
	public List<Order> findByOrderProcessType(@RequestParam OrderProcessType type) {
		return orderService.findByOrderProcessType(type);
	}
}
