package io.namoosori.shop.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;
import io.namoosori.shop.service.OrderService;
import io.namoosori.shop.store.OrderStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderServiceLogic implements OrderService{
	private final OrderStore orderStore;

	@Override
	public String registerOrder(Order order) {
		return orderStore.create(order);
	}

	@Override
	public Order findOrder(String orderId) {
		return orderStore.retrieve(orderId);
	}

	@Override
	public void modify(Order order) {
		orderStore.update(order);
	}

	@Override
	public void remove(String orderId) {
		orderStore.delete(orderId);
	}

	@Override
	public List<Order> findByOrderProcessType(OrderProcessType type) {
		return orderStore.retrieveByOrderProcessType(type);
	}

	@Override
	public List<Order> findAll() {
		return orderStore.retrieveAll();
	}

	
}
