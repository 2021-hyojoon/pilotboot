package io.namoosori.shop.logic;

import org.springframework.stereotype.Service;

import io.namoosori.shop.entity.Member;
import io.namoosori.shop.service.MemberService;
import io.namoosori.shop.store.MemberStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MemberServiceLogic implements MemberService{
	private final MemberStore memberStore;

	@Override
	public String register(Member member) {
		return memberStore.create(member);
	}

	@Override
	public Member find(String id) {
		return memberStore.retrieve(id);
	}

	@Override
	public void modify(Member member) {
		memberStore.update(member);
	}

	@Override
	public void remove(String id) {
		memberStore.delete(id);
	}
	

}
