package io.namoosori.shop.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;
import io.namoosori.shop.service.ProductService;
import io.namoosori.shop.store.ProductStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceLogic implements ProductService{
	private final ProductStore productstore;

	@Override
	public String registerProduct(Product product) {
		return productstore.create(product);
	}

	@Override
	public Product findProduct(String productId) {
		return productstore.retrieve(productId);
	}

	@Override
	public void modify(Product product) {
		productstore.update(product);
	}

	@Override
	public void remove(String productId) {
		productstore.delete(productId);
	}

	@Override
	public List<Product> findByCategory(Category category) {
		return productstore.retrieveByCategory(category);
	}

	@Override
	public List<Product> findAllClub() {
		return productstore.retrieveAllClub();
	}

	@Override
	public Product findProductName(String productName) {
		return productstore.retrieveName(productName);
	}
	
}
