package io.namoosori.shop.service;

import java.util.List;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;

public interface OrderService {
	//
	String registerOrder(Order order);

	Order findOrder(String orderId);
	
	List<Order> findByOrderProcessType(OrderProcessType type);
	
	List<Order> findAll();

	void modify(Order order);

	void remove(String orderId);

}
