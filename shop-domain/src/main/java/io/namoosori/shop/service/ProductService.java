package io.namoosori.shop.service;

import java.util.List;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;

public interface ProductService {
	//
	String registerProduct(Product product);

	Product findProduct(String productId);
	
	List<Product> findByCategory(Category category);
	
	List<Product> findAllClub();
	
	Product findProductName(String productName);


	void modify(Product product);

	void remove(String productId);

}
