package io.namoosori.shop.service;

import io.namoosori.shop.entity.Member;

public interface MemberService {
	//
	String register(Member member);

	Member find(String id);

	void modify(Member member);

	void remove(String id);
}
