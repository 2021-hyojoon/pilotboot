package io.namoosori.shop.entity;

public enum Category {
	Food,
	Clothes,
	Electronics,
	Sports
}
