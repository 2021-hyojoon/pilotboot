package io.namoosori.shop.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Order {
	private String orderId; // key tinyint AUTO_INCREMENT
	private String userId; // VARCHAR(10) FOREIGN KEY
	private String productId; // tinyint  FOREIGN KEY
	private String count; // VARCHAR(10)  FOREIGN KEY
	private String price;
	private String orderDay; // timestamp
	private Address address; 		// TEXT
	private OrderProcessType orderProcessType; // VARCHAR(10)
	private Member member;
}