package io.namoosori.shop.entity;

public class Address {
	//
	private String zipCode; 
	private String zipAddress; 
	private String streetAddress;
	private String country; 
	
	public Address() {
		//
	}

	public Address(String zipCode, String zipAddress, String streetAddress) {
		// 
		this.zipCode = zipCode; 
		this.zipAddress = zipAddress; 
		this.streetAddress = streetAddress; 
		this.country = "South Korea"; 
	}
	
	@Override
	public String toString() {
		// 
		StringBuilder builder = new StringBuilder(); 
		
		builder.append("ZipCode:").append(zipCode); 
		builder.append(", zip address:").append(zipAddress); 
		builder.append(", street address:").append(streetAddress); 
		builder.append(", country:").append(country); 
	
		return builder.toString(); 
	}
	
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getZipAddress() {
		return zipAddress;
	}

	public void setZipAddress(String zipAddress) {
		this.zipAddress = zipAddress;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
