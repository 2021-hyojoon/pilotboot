package io.namoosori.shop.entity;

public enum OrderProcessType {
	//
	Ready,
	Shipped,
	Delivered,
	Cancel
}
