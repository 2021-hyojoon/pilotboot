package io.namoosori.shop.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Member {
	private String id;		// key   // VARCHAR(10)
	private String password; 			// VARCHAR(10)
	private String name; 				// VARCHAR(10)
	private String phoneNumber; 		// VARCHAR(15)
	private String birthDay; 			// VARCHAR(20)
	private Address address; 		//
	private Permission permission;
	
	@Override
	public String toString() {
		// 
		StringBuilder builder = new StringBuilder(); 
		
		builder.append("Name:").append(name); 
		builder.append(", id:").append(id); 
		builder.append(", password:").append(password); 
		builder.append(", phone number:").append(phoneNumber); 
		builder.append(", birthDay:").append(birthDay); 
		
		return builder.toString(); 
	}
	
}
