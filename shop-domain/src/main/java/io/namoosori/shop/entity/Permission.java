package io.namoosori.shop.entity;

public enum Permission {
	Member,
	Admin
}
