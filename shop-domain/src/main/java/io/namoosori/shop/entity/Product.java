package io.namoosori.shop.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	private String productId; //key tinyint AUTO_INCREMNET
	private String productName; // VARCHAR(15)
	private String intro; // VARCHAR(255)
	private String price; // VARCHAR(20)
	private String count; // VARCHAR(10)
	private Category category;
	private String image;
}
