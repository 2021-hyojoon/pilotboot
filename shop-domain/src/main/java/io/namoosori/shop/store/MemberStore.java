package io.namoosori.shop.store;

import io.namoosori.shop.entity.Member;

public interface MemberStore {
	public String create(Member member);
	public Member retrieve(String id); 
	public void update(Member member); 
	public void delete(String id); 
	public boolean exists(String id);
}
