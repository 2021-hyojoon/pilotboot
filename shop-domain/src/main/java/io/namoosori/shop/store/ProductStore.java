package io.namoosori.shop.store;

import java.util.List;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;

public interface ProductStore {
	public String create(Product product);
	public Product retrieve(String productid); 
	public List<Product> retrieveByCategory(Category category);
	public List<Product> retrieveAllClub();
	public Product retrieveName(String productName);
	public void update(Product product); 
	public void delete(String productid); 
	public boolean exists(String productid);
}
