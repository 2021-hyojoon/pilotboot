package io.namoosori.shop.store;

import java.util.List;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;

public interface OrderStore {
	public String create(Order order);
	public Order retrieve(String orderid); 
	public List<Order> retrieveByOrderProcessType(OrderProcessType type);
	public List<Order> retrieveAll();
	public void update(Order order); 
	public void delete(String orderid); 
	public boolean exists(String orderid);
}
