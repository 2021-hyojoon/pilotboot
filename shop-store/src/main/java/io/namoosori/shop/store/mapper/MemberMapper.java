package io.namoosori.shop.store.mapper;


import org.apache.ibatis.annotations.Mapper;

import io.namoosori.shop.entity.Member;


@Mapper
public interface MemberMapper {
    //
    int create(Member member);
    Member retrieve(String id);
    int update(Member member);
    int delete(String id);
    int exists(String id);
}
