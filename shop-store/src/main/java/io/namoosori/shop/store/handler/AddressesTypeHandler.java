package io.namoosori.shop.store.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import io.namoosori.shop.entity.Address;
import io.namoosori.shop.util.JsonUtil;


public class AddressesTypeHandler extends BaseTypeHandler<Address> {

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Address address, JdbcType jdbcType) throws SQLException {
        //
        preparedStatement.setString(i, JsonUtil.toJson(address));
    }

    @Override
    public Address getNullableResult(ResultSet resultSet, String s) throws SQLException {
        //
        return Optional.ofNullable(resultSet.getString(s))
                    .map(json -> JsonUtil.fromJson(json, Address.class))
                    .orElse(new Address());
    }

    @Override
    public Address getNullableResult(ResultSet resultSet, int i) throws SQLException {
        //
        return Optional.ofNullable(resultSet.getString(i))
                .map(json -> JsonUtil.fromJson(json, Address.class))
                .orElse(new Address());
    }

    @Override
    public Address getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        //
        return Optional.ofNullable(callableStatement.getString(i))
                .map(json -> JsonUtil.fromJson(json, Address.class))
                .orElse(new Address());
    }
}
