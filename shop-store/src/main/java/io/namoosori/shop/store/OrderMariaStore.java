package io.namoosori.shop.store;

import java.util.List;

import org.springframework.stereotype.Repository;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;
import io.namoosori.shop.store.mapper.OrderMapper;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class OrderMariaStore implements OrderStore{
	private final OrderMapper orderMapper;
	
	@Override
	public String create(Order order) {
		String name = order.getOrderId();
		orderMapper.create(order);
		return name;
	}

	@Override
	public Order retrieve(String orderid) {
		return orderMapper.retrieve(orderid);
	}

	@Override
	public void update(Order order) {
		orderMapper.update(order);
	}

	@Override
	public void delete(String orderid) {
		orderMapper.delete(orderid);
	}

	@Override
	public boolean exists(String orderid) {
		boolean isExists = false;
		int result = orderMapper.exists(orderid);
        if(result != 0){
            isExists = true;
        }
		return isExists;
	}

	@Override
	public List<Order> retrieveByOrderProcessType(OrderProcessType type) {
		return orderMapper.retrieveByOrderProcessType(type);
	}

	@Override
	public List<Order> retrieveAll() {
		return orderMapper.retrieveAll();
	}

}
