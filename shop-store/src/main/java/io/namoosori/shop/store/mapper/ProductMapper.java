package io.namoosori.shop.store.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;

@Mapper
public interface ProductMapper {
    //
    int create(Product product);
    Product retrieve(String productId);
    List<Product> retrieveByCategory(Category category);
    List<Product> retrieveAllClub();
    Product retrieveName(String productName);
    int update(Product product);
    int delete(String productId);
    int exists(String productId);
}
