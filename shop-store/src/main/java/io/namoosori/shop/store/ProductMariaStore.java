package io.namoosori.shop.store;

import java.util.List;

import org.springframework.stereotype.Repository;

import io.namoosori.shop.entity.Category;
import io.namoosori.shop.entity.Product;
import io.namoosori.shop.store.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ProductMariaStore implements ProductStore{
	private final ProductMapper productMapper;
	
	@Override
	public String create(Product product) {
		String name = product.getProductName();
		productMapper.create(product);
		return name;
	}

	@Override
	public Product retrieve(String productid) {
		return productMapper.retrieve(productid);
	}

	@Override
	public void update(Product product) {
		productMapper.update(product);
	}

	@Override
	public void delete(String productid) {
		productMapper.delete(productid);
	}

	@Override
	public boolean exists(String productid) {
		boolean isExists = false;
		int result = productMapper.exists(productid);
        if(result != 0){
            isExists = true;
        }
		return isExists;
	}

	@Override
	public List<Product> retrieveByCategory(Category category) {
		return productMapper.retrieveByCategory(category);
	}

	@Override
	public List<Product> retrieveAllClub() {
		return productMapper.retrieveAllClub();
	}

	@Override
	public Product retrieveName(String productName) {
		return productMapper.retrieveName(productName);
	}
	

}
