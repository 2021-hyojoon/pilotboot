package io.namoosori.shop.store;

import org.springframework.stereotype.Repository;

import io.namoosori.shop.entity.Member;
import io.namoosori.shop.store.mapper.MemberMapper;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class MemberMariaStore implements MemberStore{
	private final MemberMapper memberMapper;

	@Override
	public String create(Member member) {
		String name = member.getName();
		memberMapper.create(member);
		return name;
	}

	@Override
	public Member retrieve(String id) {
		return memberMapper.retrieve(id);
	}

	@Override
	public void update(Member member) {
		memberMapper.update(member);
	}

	@Override
	public void delete(String id) {
		memberMapper.delete(id);
	}

	@Override
	public boolean exists(String id) {
		boolean isExists = false;
		int result = memberMapper.exists(id);
        if(result != 0){
            isExists = true;
        }
		return isExists;
	}
	
}
