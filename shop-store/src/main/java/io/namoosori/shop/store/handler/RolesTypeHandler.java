package io.namoosori.shop.store.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import io.namoosori.shop.util.JsonUtil;

public class RolesTypeHandler extends BaseTypeHandler<List<String>>{

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setString(i, JsonUtil.toJson(parameter));
	}

	@Override
	public List<String> getNullableResult(ResultSet rs, String s) throws SQLException {
        return Optional.ofNullable(rs.getString(s))
                .map(json -> JsonUtil.fromJsonList(json, String.class))
                .orElse(new ArrayList<>());
	}

	@Override
	public List<String> getNullableResult(ResultSet rs, int s) throws SQLException {
		return Optional.ofNullable(rs.getString(s))
                .map(json -> JsonUtil.fromJsonList(json, String.class))
                .orElse(new ArrayList<>());
	}

	@Override
	public List<String> getNullableResult(CallableStatement cs, int s) throws SQLException {
		return Optional.ofNullable(cs.getString(s))
                .map(json -> JsonUtil.fromJsonList(json, String.class))
                .orElse(new ArrayList<>());
	}
	

}
