package io.namoosori.shop.store.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import io.namoosori.shop.entity.Order;
import io.namoosori.shop.entity.OrderProcessType;

@Mapper
public interface OrderMapper {
    //
    int create(Order order);
    Order retrieve(String orderId);
    List<Order> retrieveByOrderProcessType(OrderProcessType type);
    List<Order> retrieveAll();
    int update(Order order);
    int delete(String orderId);
    int exists(String orderId);
}
